import { useState, useEffect } from "react";
import styled from "styled-components";
import PropTypes from 'prop-types';

const ModalWrapper = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: 100;
    background-color: rgba(0,0,0,0.4);
`
const ModalToBuyElement = styled.div`
    max-width: 500px;
    box-sizing: border-box;
    margin: 0 auto;
    border: 1px solid;
    border-radius: 5px;
    border-width: 0px;
    background-color: #c84141;
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
`
const HeaderModal = styled.div`
    display: flex;
    justify-content: space-between;
    padding: 14px;
    background: #9a2828;
    align-items: center;
`
const HeaderModalQuestion = styled.p`
    color: white;
`
const CloseModal = styled.div`
    cursor: pointer;
    width: 24px;
    height: 4px;

    &:before,
    &:after {
        content: "";
        position: absolute;
        width: 24px;
        height: 1.5px;
        background: white;
    }

    &:before {
        transform: rotate(45deg);
    }

    &:after {
        transform: rotate(-45deg);
    }
`

const InformPartOfModal = styled.div`

`
const InformText = styled.div`
    color: white;
    margin: 28px 22px 14px;
    text-align: center;
`
const BtnsModalClose = styled.div`
    display: flex;
    justify-content: center;
    gap: 30px;
    padding: 20px;
`

const ButtonModalAgree = styled.button`
    padding: 10px 46px;
    background: #626262;
    border-width: 0px;
    color: white;
    border-radius: 4px;
`
const BtnModalDisagree = styled.button`
    padding: 10px 46px;
    background: #626262;
    border-width: 0px;
    color: white;
    border-radius: 4px;
`


function ModalConfirmToBuy({closeModalToBuy, putElementIntoBascket}) {
    return (
        <>
            <ModalWrapper id="modal-wrapper" onClick={closeModalToBuy}>
                <ModalToBuyElement>
                    <HeaderModal>
                        <HeaderModalQuestion>Do you want to buy this item?</HeaderModalQuestion>
                        <CloseModal data-close = 'true' onClick={closeModalToBuy} />
                    </HeaderModal>
                    <InformPartOfModal>
                        <InformText>Are you sure you want to buy this?</InformText>
                        <BtnsModalClose>
                            <ButtonModalAgree onClick={putElementIntoBascket}>Ok</ButtonModalAgree>
                            <BtnModalDisagree data-close = 'true' onClick={closeModalToBuy}>Cancel</BtnModalDisagree>
                        </BtnsModalClose>
                    </InformPartOfModal>
                </ModalToBuyElement>
            </ModalWrapper>
        </>
    )
}

export default ModalConfirmToBuy;
