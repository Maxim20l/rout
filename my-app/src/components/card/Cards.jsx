import styled from "styled-components";
import { Container } from "../../Container";
import PropTypes from 'prop-types';
import { Card } from "./components/card";


const ContainerCards = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    gap: 8px
`


function Cards({cardsArr, openModalToBuy, arrProductsFavorite, chooseFavoriteProdoct}) {

    const card = cardsArr.map(elem => {
        return <Card key={elem.articul} data={elem} chooseFavoriteProdoct={chooseFavoriteProdoct} arrProductsFavorite={arrProductsFavorite} openModalToBuy={openModalToBuy} />;
    })
    return(
        <>
            <Container>
                    <ContainerCards>
                        {card}
                    </ContainerCards>
                </Container>
        </>
    )
}


Cards.propTypes = {
    cardsArr: PropTypes.array,
    isFavorite: PropTypes.bool,
    isUnFavorite: PropTypes.bool,
    changeStarFavorite: PropTypes.func,
    arrProductsFavorite: PropTypes.string,
    openModalToBuy: PropTypes.func
}

export default Cards;
