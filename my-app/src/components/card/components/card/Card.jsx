import styled from "styled-components";
import { useState, useEffect } from 'react';
import { IoStar, IoStarOutline } from "react-icons/io5";
import PropTypes from 'prop-types';
import { BtnOpenModal } from "../../../btnOpenModal"



const Container = styled.div`
    background: #e5d1ca;
    width: 250px;
    display: flex;
    flex-direction: column;
    align-items: center;
    border: 1px solid black;
    border-width: 0px;
    box-shadow: 7px 6px 4px grey;
    border-radius: 10px;
    margin-top: 10px;
    position: relative;
`

const Image = styled.img`
    width: 200px;
    height: 200px;
    border: 1px solid black;
    border-width: 0px;
    border-radius: 80px;
`
const WrapperImg = styled.div`
    padding: 10px;
`
const Star = styled.div`
    width: 25px;
    position: absolute;
    right: 20px
`
const NameIceCream = styled.p`
    text-align: center;
    color: #CC2A41;
    margin: 8px;
    font-size: 18px
`
const PriceIceCream = styled.p`
    text-align: center;
    color: black;
    margin: 8px;
    font-size: 16px
`

const ColorIceCream = styled.p`
    text-align: center;
    color: black;
    margin: 8px;
    font-size: 16px
`
function Card(props) {
    const {
        name,
        price,
        img,
        articul,
        color
    } = props.data;
    const {openModalToBuy, arrProductsFavorite, chooseFavoriteProdoct} = props;

    return (
        <>
            <Container id={articul}>
                <WrapperImg>
                    <Star onClick={chooseFavoriteProdoct}>
                        {arrProductsFavorite.includes(articul) ? <IoStar/> : <IoStarOutline/>}
                    </Star>
                    <Image src={img} alt={color}></Image>
                </WrapperImg>
                <div className="inform">
                    <NameIceCream>{name}</NameIceCream>
                    <PriceIceCream>{price}</PriceIceCream>
                    <ColorIceCream>{color}</ColorIceCream>
                </div>
                <BtnOpenModal openModalToBuy={openModalToBuy}/>
            </Container>
        </>
    )
}

export default Card;