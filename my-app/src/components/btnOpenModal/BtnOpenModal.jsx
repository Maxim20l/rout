import styled from "styled-components";
import PropTypes from 'prop-types';

const BtnBuy = styled.button`
    text-decoration: none;
    border: 1px solid black;
    margin: 10px;
    background: #cc2a41;
    padding: 10px 30px;
    border-width: 0px;
    border-radius: 10px;
    color: white;
    font-family: 'Roboto';
    transition: 0.9s;

    &:hover {
        color: #CC2A41;
        background: white;
    }
`


function BtnOpenModal({ openModalToBuy }) {
    return (

        <BtnBuy onClick={openModalToBuy}>Buy</BtnBuy>

    )
}


BtnOpenModal.propTypes = {
    openModalToBuy: PropTypes.func,
}

export default BtnOpenModal;