import styled from "styled-components";
import PropTypes from 'prop-types';

const CountFav = styled.div`
    position: absolute;
    bottom: 8px;
    left: 55px
`;


function CounterFavorite({counterFavorite}) {
  const actualCount = () => {
        if (localStorage.getItem('arrayFavorite') === null) {
            return counterFavorite;
        }
        return JSON.parse(localStorage.getItem('arrayFavorite')).length;
    }

    return (
        <CountFav>
            <p>{actualCount()}</p>
        </CountFav>
    )

}


CounterFavorite.propTypes = {
    countFavorite: PropTypes.number,
}
export default CounterFavorite;