import styled from "styled-components";
import { useState, useEffect } from "react";


const FooterContainer = styled.footer`
    background:  #FEC77E;
    margin-top: 14px;
`;
const FooterText = styled.p`
    text-align: center;
    color: white;
    padding: 12px;
    color: #CC2A41;
    font-size: 18px;
`
function Footer() {

    return (
        <>
            <FooterContainer>
                <FooterText>
                    © 2022 by The Magic Slab.
                </FooterText>
            </FooterContainer>
        </>
    )
}


export default Footer;