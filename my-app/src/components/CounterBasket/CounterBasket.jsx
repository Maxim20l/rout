import styled from "styled-components";
import PropTypes from 'prop-types';


const CountBasket = styled.div`
    position: absolute;
    bottom: 8px;
    left: 20px
`;


function CounterBasket({countBasket}) {
    const actualCount = () => {
        if (localStorage.getItem('arrayBasket') === null) {
            return countBasket;
        }
        return JSON.parse(localStorage.getItem('arrayBasket')).length;
      
    }
    return (
        <>
            <CountBasket>
                <p>{actualCount()}</p>
            </CountBasket>
        </>
    )
}

CounterBasket.propTypes = {
    countBasket: PropTypes.number,
}

export default CounterBasket;