import './App.css';
import { Header } from './components/header';
import { useEffect, useState } from 'react';
import Cards from './components/card/Cards';
import { Footer } from './components/footer';
import { ModalConfirmToBuy } from './components/modalConfirmToBuy';

function App() {

  let arrFavorite = () => {
    if (localStorage.getItem('arrayFavorite') === null) {
      return '[]';
    }
    return localStorage.getItem('arrayFavorite');
  }

  const [arrProductsFavorite, setArrProductsFavorite] = useState(arrFavorite());
  const [isModalToBuy, setIsModalToBuy] = useState(false);
  const [arrCards, setArrCards] = useState([]);
  const [productToBuy, setProductToBuy] = useState(0);
  const [countBasket, setCountBasket] = useState(0);
  const [counterFavorite, setCounterFavorite] = useState(0);



  useEffect(() => {
    const sendRequest = async (url) => {
      const response = await fetch(url);
      const result = await response.json();
      setArrCards(result.product);
      return result.product;
    }
    sendRequest('./iceCreamProducts.json');
  }, [])

  const openModalToBuy = (event) => {
    setProductToBuy(event.target.parentElement.id);
    setIsModalToBuy(!isModalToBuy);
  }

  const closeModalToBuy = (event) => {
    event.stopPropagation();
    if (event.target.id === 'modal-wrapper') {
      setIsModalToBuy(!isModalToBuy);
    } else if (event.target.dataset.close === 'true') {
      setIsModalToBuy(!isModalToBuy);
    }
  }

  const chooseFavoriteProdoct = (event) => {
    if (!localStorage.getItem('arrayFavorite')) {
      localStorage.setItem('arrayFavorite', JSON.stringify([]));
    }

    if (event.target.parentElement.tagName == 'svg') {
      const parentImg = event.target.parentElement;
      const parentParentImg = parentImg.parentElement;
      const wrapperParentimg = parentParentImg.parentElement;
      const product = wrapperParentimg.parentElement.id;
      const newArr = JSON.parse(arrProductsFavorite);
      if (!JSON.parse(arrProductsFavorite).includes(product)) {
        newArr.push(product);
      }
      localStorage.setItem('arrayFavorite', JSON.stringify(newArr));
      setArrProductsFavorite(localStorage.getItem('arrayFavorite'));
    } else {
      const parentImg = event.target.parentElement;
      const parentParentImg = parentImg.parentElement;
      const product = parentParentImg.parentElement.id;
      const newArr = JSON.parse(arrProductsFavorite);
      if (!JSON.parse(arrProductsFavorite).includes(product)) {
        newArr.push(product);
      }
      localStorage.setItem('arrayFavorite', JSON.stringify(newArr));
      setArrProductsFavorite(localStorage.getItem('arrayFavorite'));
    }
  }

  const putElementIntoBascket = () => {
    if (localStorage.getItem('arrayBasket') === null) {
      localStorage.setItem('arrayBasket', JSON.stringify([]));
    }
    const arrProductsToBuy = JSON.parse(localStorage.getItem('arrayBasket'));
    arrProductsToBuy.push(productToBuy);
    localStorage.setItem('arrayBasket', JSON.stringify(arrProductsToBuy));
    setIsModalToBuy(!isModalToBuy);
  }


  return (
    <>
      <Header countBasket={countBasket} counterFavorite={counterFavorite} />
      <Cards chooseFavoriteProdoct={chooseFavoriteProdoct} arrProductsFavorite={arrProductsFavorite} cardsArr={arrCards} openModalToBuy={openModalToBuy} />
      <Footer />
      {isModalToBuy && <ModalConfirmToBuy closeModalToBuy={closeModalToBuy} putElementIntoBascket={putElementIntoBascket} />}
    </>
  );
}

export default App;
